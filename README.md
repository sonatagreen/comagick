# **Com**ag**ic**k
## comagick 0.4.1

Comagick (pronounced COM-magic) is an ImageMagick-based language for writing sprite comics.

This repository contains just the Comagick software tools.

To get started using Comagick, follow the following steps:

1. Make sure you have the prerequisites installed: Python 3 and [Wand](http://docs.wand-py.org/en/0.4.1/).
2. Download this repository, and place the files `comagick` and `decompose-spritesheet` somewhere in your [PATH](https://en.wikipedia.org/wiki/PATH_%28variable%29).
3. Fork [comagick-project](https://bitbucket.org/sonatagreen/comagick-project).
4. Navigate to the comagick-project directory and run `make`.
5. Create a new file at `comics/<comicname>.cmg`, or open up `comics/example.cmg`.

----

Usage:

    decompose-spritesheet sheetname
    comagick input.cmg output.png

----

Comagick is released under AGPLv3+. See LICENSE for more information.
